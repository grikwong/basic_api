package model

import (
	"errors"
	"sync"
	"time"
)

type User struct {
	ID        int
	Name      string
	CreatedAt string
	UpdatedAt string
}

type Users []User

const (
	standardDateTimeFormat = "2006-01-02 15:04:05"
)

var (
	// global variable for users - serves as in-memory persistence
	users Users

	// global variable for id - serves as in-memory persistence for tracking of id autoincrement
	userID int

	// global variable for user mutex
	userMutex sync.Mutex

	// global variable for userID mutex
	userIDMutex sync.Mutex
)

// init function ...
func init() {
	currentDateTime := time.Now().Format(standardDateTimeFormat)

	users = Users{
		User{
			ID:        getID(),
			Name:      "Alice",
			CreatedAt: currentDateTime,
			UpdatedAt: currentDateTime,
		},
		User{
			ID:        getID(),
			Name:      "Bob",
			CreatedAt: currentDateTime,
			UpdatedAt: currentDateTime,
		},
	}
}

// AddUser function ...
func AddUser(name string) {
	userMutex.Lock()
	defer userMutex.Unlock()

	currentDateTime := time.Now().Format(standardDateTimeFormat)

	users = append(users, User{
		ID:        getID(),
		Name:      name,
		CreatedAt: currentDateTime,
		UpdatedAt: currentDateTime,
	})

	return
}

// GetUsers function ...
func GetUsers() (u Users) {
	return users
}

// FindUserByID function ...
func FindUserByID(id int) (u User, err error) {
	for _, v := range users {
		if v.ID == id {
			return v, nil
		}
	}

	err = errors.New("FindUserByID error: Record not found")
	return
}

// FindUserByName function ...
func FindUserByName(name string) (u User, err error) {
	for _, v := range users {
		if v.Name == name {
			return v, nil
		}
	}

	err = errors.New("FindUserByName error: Record not found")
	return
}

// Update method ...
func (me *User) Update() error {
	userMutex.Lock()
	defer userMutex.Unlock()

	for k, v := range users {
		if v.ID == me.ID {
			newUser := *me
			newUser.UpdatedAt = time.Now().Format(standardDateTimeFormat)
			users[k] = newUser
			return nil
		}
	}

	return errors.New("Update error: Record not found")
}

// Delete method ...
func (me *User) Delete() error {
	userMutex.Lock()
	defer userMutex.Unlock()

	for k, v := range users {
		if v.ID == me.ID {
			/**
				// index = 0
				users[1:]

				// index = 1
				users[0:0] + users[2:]

				// index = 2
				users[0:1] + users[3:]
			*/
			// prepares the new users data
			if k == 0 {
				users = users[k+1:]
			} else {
				users = append(users[0:k], users[k+1:]...)
			}

			return nil
		}
	}

	return errors.New("Delete error: Record not found")
}

// getID function - increments current id then returns it
func getID() (id int) {
	userIDMutex.Lock()
	defer userIDMutex.Unlock()

	userID += 1
	id = userID

	return
}
