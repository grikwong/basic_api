package main

import "bitbucket.org/grikwong/basic_api/api"

func main() {
	// init api application
	app := api.New()

	// start listener
	api.Listen(app)
}
