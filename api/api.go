package api

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/grikwong/basic_api/api/resource"
	"github.com/gorilla/mux"
)

const (
	hostEnv = "APP_HOST"
	portEnv = "APP_PORT"
)

// New function - returns prepared router
func New() *mux.Router {
	log.Printf("Initializing API application ...")

	// prepares router
	router := mux.NewRouter()
	router.HandleFunc(resource.UserResourceEndpoint, resource.GetUserController).Methods(http.MethodGet)
	router.HandleFunc(resource.UserResourceEndpoint, resource.PostUserController).Methods(http.MethodPost)
	router.HandleFunc(resource.UserWildCardResourceEndpoint, resource.PutUserController).Methods(http.MethodPut)
	router.HandleFunc(resource.UserWildCardResourceEndpoint, resource.DeleteUserController).Methods(http.MethodDelete)

	return router
}

// Listen function - perpares and starts the listener
func Listen(router *mux.Router) {
	host := os.Getenv(hostEnv)
	port := os.Getenv(portEnv)

	// setting default value for host var
	if host == "" {
		host = "127.0.0.1"
	}

	// setting default value for port var
	if port == "" {
		port = "8081"
	}

	// start the listener
	fullUrl := host + ":" + port
	log.Printf("Listening on `%s` ...", fullUrl)

	if err := http.ListenAndServe(fullUrl, router); err != nil {
		panic("ListenAndServe error: " + err.Error())
	}
}
