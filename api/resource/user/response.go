package user

type GetResponse struct {
	Users []User `json:"users"`
}

type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// GetSuccessResponse function - returns formatted success response object
func GetSuccessResponse(message string) Response {
	return Response{
		Success: true,
		Message: message,
	}
}
