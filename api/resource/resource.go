package resource

import (
	"encoding/json"
	"net/http"

	resError "bitbucket.org/grikwong/basic_api/api/resource/error"
)

// writeErrorResponse function - formats and writes error response
func writeErrorResponse(statusCode int, message string, w http.ResponseWriter) {
	w.Header().Set(contentTypeHeader, contentTypeJson) // set response content-type to application/json
	w.WriteHeader(statusCode)                          // set status code

	if err := json.NewEncoder(w).Encode(resError.FormatResponse(message)); err != nil {
		panic(err.Error())
	}
}

// writeSuccessResponse function - formats and writes success response
func writeSuccessResponse(statusCode int, response interface{}, w http.ResponseWriter) {
	w.Header().Set(contentTypeHeader, contentTypeJson) // set response content-type to application/json
	w.WriteHeader(statusCode)                          // set status code

	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err.Error())
	}
}
