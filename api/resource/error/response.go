package error

type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

// FormatResponse function - returns formatted error object
func FormatResponse(message string) Response {
	return Response{
		Success: false,
		Message: message,
	}
}
