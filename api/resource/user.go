package resource

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/grikwong/basic_api/api/resource/user"
	"bitbucket.org/grikwong/basic_api/model"
	"github.com/gorilla/mux"
)

const (
	UserResourceEndpoint         = "/user"
	UserWildCardResourceEndpoint = "/user/{" + userWildCard + "}"

	userWildCard = "id"

	contentTypeHeader = "Content-Type"
	contentTypeJson   = "application/json"

	postUserMessage   = "User successfully added"
	putUserMessage    = "User successfully updated"
	deleteUserMessage = "User successfully deleted"
)

// GetUserController function -
func GetUserController(w http.ResponseWriter, r *http.Request) {
	var (
		res        user.GetResponse
		modelUsers = model.GetUsers()
	)

	// map user model object to user response object
	for _, v := range modelUsers {
		res.Users = append(res.Users, user.User{
			ID:        v.ID,
			Name:      v.Name,
			CreatedAt: v.CreatedAt,
			UpdatedAt: v.UpdatedAt,
		})
	}

	writeSuccessResponse(http.StatusOK, res, w)
	return
}

// PostUserController function -
func PostUserController(w http.ResponseWriter, r *http.Request) {
	var (
		req user.PostRequest
		res = user.GetSuccessResponse(postUserMessage)
	)

	// reads entire request body stream
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	// maps request body stream to expected structure
	if err = json.Unmarshal(body, &req); err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	// calling model to handle add new user record
	model.AddUser(req.Name)

	writeSuccessResponse(http.StatusOK, res, w)
	return
}

// PutUserController function -
func PutUserController(w http.ResponseWriter, r *http.Request) {
	var (
		req user.PutRequest
		res = user.GetSuccessResponse(putUserMessage)

		userID = 0
		err    error
	)

	// checks if id wildcard is not empty
	if mux.Vars(r)[userWildCard] == "" {
		writeErrorResponse(http.StatusBadRequest, "Path wildcard value `id` is required", w)
		return
	} else {
		if userID, err = strconv.Atoi(mux.Vars(r)[userWildCard]); err != nil {
			writeErrorResponse(http.StatusBadRequest, "Path wildcard value `id` contains invalid value", w)
			return
		}
	}

	// reads entire request body stream
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	// maps request body stream to expected structure
	if err = json.Unmarshal(body, &req); err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	// calling model to handle search and update user record
	retrievedUser, err := model.FindUserByID(userID)

	if err != nil {
		writeErrorResponse(http.StatusBadRequest, err.Error(), w)
		return
	}

	retrievedUser.Name = req.Name
	err = retrievedUser.Update()

	if err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	writeSuccessResponse(http.StatusOK, res, w)
	return
}

// DeleteUserController function -
func DeleteUserController(w http.ResponseWriter, r *http.Request) {
	var (
		res = user.GetSuccessResponse(deleteUserMessage)

		userID = 0
		err    error
	)

	// checks if id wildcard is not empty
	if mux.Vars(r)[userWildCard] == "" {
		writeErrorResponse(http.StatusBadRequest, "Path wildcard value `id` is required", w)
		return
	} else {
		if userID, err = strconv.Atoi(mux.Vars(r)[userWildCard]); err != nil {
			writeErrorResponse(http.StatusBadRequest, "Path wildcard value `id` contains invalid value", w)
			return
		}
	}

	// calling model to handle search and delete user record
	retrievedUser, err := model.FindUserByID(userID)

	if err != nil {
		writeErrorResponse(http.StatusBadRequest, err.Error(), w)
		return
	}

	err = retrievedUser.Delete()

	if err != nil {
		writeErrorResponse(http.StatusInternalServerError, err.Error(), w)
		return
	}

	writeSuccessResponse(http.StatusOK, res, w)
	return
}
